package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User user){
        userService.createUser(user);
        return new ResponseEntity<>("User created successfully.", HttpStatus.CREATED);
    }

    @RequestMapping(value="/users", method = RequestMethod.GET)
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/users/{userid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long userid){
        return  userService.deleteUser(userid);
    }

    @RequestMapping(value = "/users/{userid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long userid, @RequestBody User user){
        return userService.updateUser(userid, user);
    }
}
