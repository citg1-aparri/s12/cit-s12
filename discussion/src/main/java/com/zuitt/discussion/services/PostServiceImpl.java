package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService{
   @Autowired
   private PostRepository postRepository;
    public void createPost(Post post) {
        postRepository.save(post);
    }
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }
    public ResponseEntity deletePost(Long id) {
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);
    }

    public ResponseEntity updatePost(Long id, Post post){
        Post postForUpdate = postRepository.findById(id).get();
        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());
        postRepository.save(postForUpdate);
        return new ResponseEntity<>("Post updated succcessfully", HttpStatus.OK);
    }


}
